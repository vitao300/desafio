<?php
require_once('/../conexao.php');

/**
 * Classe que manipula a listagem dos planetas
 *
 * Class Listagem
 */
class Listagem
{
    private $listagemPesquisa;
    private $erro = false;

    /**
     * M�todo que retorna a listagem feita pelo campo de pesquisa
     *
     * @param $link Conex�o
     * @param $dados Dados da consulta
     * @return bool|mixed
     */
    public function listarPesquisa($link, $dados){
        if(!empty($dados['pesquisar'])){
            $id = explode('-', $dados['pesquisar']);
            $sql = "SELECT * FROM planetas WHERE id = $id[0]";
            $res = mysqli_query($link, $sql);
            if(!$res){
                $this->erro = true;
                return $this->erro;
            }
            while($row = mysqli_fetch_assoc($res)){
                $this->setListagemPesquisa($row);
            }
            return $this->getListagemPesquisa();
        }
    }

    /**
     * M�todo que retorna todos os dados
     *
     * @param $link Conex�o
     * @return string
     */
    public function listarTudo($link, $json = false){
        $sql = "SELECT * FROM planetas";
        $res = mysqli_query($link, $sql);
        if(!$res){
            $this->erro = true;
            return $this->erro;
        }
        $array = [];
        if($json == true){
            while($row = mysqli_fetch_assoc($res)){
                // Prepara o array para ser retornado um Json caso campo de pesquisa esteja preenchido
                $array[] = ['value' => $row['id'].'-'.$row['nome'], 'label' => $row['id'].'-'.$row['nome']];
            }
            return json_encode($array);
        }

        // Array de quando a pesquisa for para retornar todos os resultados
        while($row = mysqli_fetch_assoc($res)){
            $array[] = $row;
        }

        return $array;
    }

    /**
     * @return mixed
     */
    public function getListagemPesquisa()
    {
        return $this->listagemPesquisa;
    }

    /**
     * @param mixed $listagemPesquisa
     */
    public function setListagemPesquisa($listagemPesquisa)
    {
        $this->listagemPesquisa = $listagemPesquisa;
    }

    /**
     * @return bool
     */
    public function isErro()
    {
        return $this->erro;
    }

    /**
     * @param bool $erro
     */
    public function setErro($erro)
    {
        $this->erro = $erro;
    }


}
