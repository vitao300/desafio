<?php
require_once('/../conexao.php');

/**
 * Classe que faz a persistência
 *
 * Class Crud
 */
class Crud
{
    public function __construct($link, $post)
    {
        if (isset($post['submit'])) {
            switch ($post['submit']) {
                case 'incluir':
                    $this->incluir($link, $post);
                    break;
                case 'deletar':
                    $this->deletar($link, $post);
                    break;
                default:
                    echo 'Ocorreu um erro';
                    die;
            }
        }
    }

    /**
     * Método de inclusão de planetas
     *
     * @param $link Conexão
     * @param $dados Dados dos planetas
     */
    private function incluir($link, $dados)
    {
        $nome = mysqli_real_escape_string($link, $dados['nome']);
        $clima = mysqli_real_escape_string($link, $dados['clima']);
        $terreno = mysqli_real_escape_string($link, $dados['terreno']);

        $sql = "INSERT INTO planetas (nome, clima, terreno) VALUES ('{$nome}', '{$clima}', '{$terreno}')";
        $res = mysqli_query($link, $sql) or die("Ocorreu um erro na inserção do planeta");
        if($res){
           header('location: ../index.php');
        }
    }

    /**
     * Método de remoção de planetas
     *
     * @param $link Conexão
     * @param $dados Id do planeta a ser excluído
     * @return bool
     */
    private function deletar($link, $dados)
    {
        $id = $dados['id'];
        $sql = "DELETE FROM planetas WHERE id = $id";
        $res = mysqli_query($link, $sql);

        if (mysqli_affected_rows($res) > 0) {
            return true;
        }
        return false;
    }
}

$crud = new Crud($link, $_POST);

