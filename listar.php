<?php
require_once('conexao.php');
require_once('Controller/Listagem.php');
require_once('Controller/Curl.php');

$lista = new Listagem();
$listagem = $lista->listarTudo($link, true);

// CURL para retornar os planetas
$curl = new Curl();
$listaCurl = $curl->consultaCurl();
$listaDecode = json_decode($listaCurl);

// Quantos planetas tem na API
$countPlanetasJson = count($listaDecode->results);

// Monta o array com a quantidade de planetas
for ($i = 0; $i < $countPlanetasJson; $i++) {
    $numFilms = count($listaDecode->results{$i}->films);
    $planetasFilms[$listaDecode->results{$i}->name] = ['films' => $numFilms];
}

if (isset($_POST['pesquisar']) and isset($_POST['submit'])) {
    $lista->listarPesquisa($link, $_REQUEST);
}

?>


<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/jquery-ui-themes-1.12.1/jquery-ui.min.css">
    <link rel="stylesheet" href="css/fontawesome-free-5.3.1-web/css/all.css">
</head>
<title>Filmes Star Wars</title>
<body>

<div class="content">
    <div class="col-md-12 text-center">
        <h1>Listagem de Planetas</h1>
    </div>
    <div class="col-md-12">
        <form action='' method="post">
            <div class="form-group col-md-5">
                <input type="text" class="form-control" id="pesquisar" name="pesquisar"
                       placeholder="Nome do planeta ou ID (Deixe em branco para listar todos)">
            </div>
            <button type="submit" class="btn btn-primary" name="submit" value="listar">Pesquisar</button>
        </form>
    </div>

    <?php
    // Caso haja algum erro na pesquisa
    if ($lista->isErro()) {
        echo '<div class="col-md-6 alert alert-danger">Nenhum planeta foi encontrado!</div>';
    } else {
        ?>

        <div class="col-md-12">
            <table class="table table-hover table-responsive">
                <th>Nome</th>
                <th>Clima</th>
                <th>Terreno</th>
                <th>Qtd Filmes</th>
                <th>Excluir</th>
                <?php
                $films = 0;

                // Lista todos os planetas
                if (empty($lista->getListagemPesquisa())) {
                    $listarTodosPesquisa = $lista->listarTudo($link, false);
                    foreach ($listarTodosPesquisa as $item) {
                        $idItem = $item['id'];
                        // Verifica em quantos filmes apareceu com o resultado retornado e tratado via curl
                        // Colocando tudo minúsculo e depois a primeira letra como uppercase para ficar no padrão da API
                        $CapitalNome = ucfirst(strtolower($item['nome']));
                        if (isset($planetasFilms[$CapitalNome])) {
                            $films = $planetasFilms[$item['nome']]['films'];
                        }
                        echo "<tr>";
                        echo "<td>" . $item['nome'] . "</td>";
                        echo "<td>" . $item['clima'] . "</td>";
                        echo "<td>" . $item['terreno'] . "</td>";
                        echo "<td>" . $films . "</td>";
                        echo "<td><i class='fa fa-trash remover' data-key='$idItem' style='color: #e1001b; cursor: pointer;' title='Exluir'></i></td>";
                        echo "<tr>";
                    }
                } else {
                    //Lista apenas o planeta escolhido
                    $id = $lista->getListagemPesquisa()['id'];
                    // Verifica em quantos filmes apareceu com o resultado retornado e tratado via curl
                    // Colocando tudo minúsculo e depois a primeira letra como uppercase para ficar no padrão da API
                    $CapitalNome = ucfirst(strtolower($lista->getListagemPesquisa()['nome']));
                    if (isset($planetasFilms[$CapitalNome])) {
                        $films = $planetasFilms[$lista->getListagemPesquisa()['nome']]['films'];
                    }
                    echo "<tr>";
                    echo "<td>" . $lista->getListagemPesquisa()['nome'] . "</td>";
                    echo "<td>" . $lista->getListagemPesquisa()['clima'] . "</td>";
                    echo "<td>" . $lista->getListagemPesquisa()['terreno'] . "</td>";
                    echo "<td>" . $films . "</td>";
                    echo "<td><i class='fa fa-trash remover' data-key='$id' style='color: #e1001b; cursor: pointer;' title='Excluir'></i></td>";
                    echo "<tr>";
                }
                ?>
            </table>
        </div>
        <?php
    }
    ?>

    <div id="response-ajax" style="display: none"></div>
</div>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
<script>
    var $local_source = '';

    $('#pesquisar').keydown(function () {
        $local_source = <?= $listagem ?>;

        $('#pesquisar').autocomplete({
            source: $local_source
        });

    });

    $('.remover').click(function () {
        var id = $(this).data('key');
        var $this = $(this);
        $.ajax({
            url: 'Controller/Crud.php',
            data: {id: id, submit: 'deletar'},
            method: 'post',
            success: function (res) {
                if (res) {
                    $('#response-ajax').html('O planeta foi excluído com sucesso!');
                    $('#response-ajax').addClass('alert alert-success');
                    $('#response-ajax').show();
                    $this.parent().parent().remove();
                } else {
                    $('#response-ajax').html('Ocorreu um problema com a exclusão do planeta');
                    $('#response-ajax').addClass('alert alert-danger');
                    $('#response-ajax').show();
                }
            }
        });
    });

</script>
</body>
</html>


