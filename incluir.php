<?php
    require_once('conexao.php');
    require_once('Controller/Crud.php');
?>



<html>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="css/bootstrap.css">
</head>
<title>Filmes Star Wars</title>
<body>

<div class="content">
    <div class="col-md-12 text-center">
        <h1>Inclusão de Planetas</h1>
    </div>
    <div class="col-md-12">
        <form action="Controller/Crud.php" method="post">
            <div class="form-group col-md-5">
                <label for="nome">Nome</label>
                <input type="text" class="form-control" required id="nome" name="nome" placeholder="Nome do planeta">
            </div>
            <div class="form-group col-md-5">
                <label for="clima">Clima</label>
                <input type="text" class="form-control" required id="clima" name="clima" placeholder="Clima">
            </div>
            <div class="form-group col-md-5">
                <label for="terreno">Terreno</label>
                <input type="text" class="form-control" required id="terreno" name="terreno" placeholder="Terreno">
            </div>
            <button type="submit" class="btn btn-primary" name="submit" value="incluir">Incluir</button>
        </form>
    </div>
</div>
</body>
</html>


